const path = require('path');

module.exports = {
  entry: ['whatwg-fetch', "./src/index.js"],
  output: {
    path: path.join(__dirname, 'public'),
    filename: "bundle.js",
    publicPath: "/"
  },
  devServer: {
    contentBase: "./public",
    hot: true,
    proxy: {
      '/api.yelp.com/v3*': 'http://localhost:8080'
    }
  },
  devtool: 'inline-source-map',
  module: {
    loaders: [
      { //loaders -- Specifies how each file should be processed before combining into bundle
      test: /\.js$/,
      exclude: /node_modules/,
      loader: "babel-loader", //convert ES6+JSX JavaScript into ES5 (which moder browsers understand)
      options: {
        presets: ["es2015", "react", "react-hmre"]
      }
    },
      {
        test: /\.(jpg|png|svg)$/,
        loader: "file-loader",
        options: {
          name: 'assets/images/[name].[ext]'
        }
      },
      {
        test: /\.scss$/,
        loaders: ['style-loader', 'css-loader', 'sass-loader']
      }
    ]
  },
  resolve: { //Where webpack should look for referenced by import or require(). Makes it so that I can import npm packages in code
    modules: [
      path.join(__dirname, 'node_modules')
    ]
  }
};