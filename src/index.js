import {render} from "react-dom";
import React from "react"
import App from "./components/App";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';


import injectTapEventPlugin from 'react-tap-event-plugin';
require('./styles/main.scss');

const MaterialUIApp = () => (
  <MuiThemeProvider>
    <App/>
  </MuiThemeProvider>


);


const containerEl = document.getElementById("app");
injectTapEventPlugin();

render(
  <div>
    <MaterialUIApp/>
  </div>,
  containerEl
);

