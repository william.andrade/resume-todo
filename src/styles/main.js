const h1 = {
  fontSize: 24,
  marginBottom: 0,
  fontWeight: 700
};

const h2 = {
  fontSize: 20,
  marginTop: 10,
  color: '#777'
};

const h3 = {
  fontSize: 16,
  fontWeight: 700,
  marginBottom: 0
};

const h4 = {
  fontSize: 15,
  fontWeight: 700
};

const divider = {
  padding: '10px 20px 0 20px',
  borderBottom: '1px solid #f0f0f0',
  marginBottom: 20
};

const sectionHeader = {
  paddingBottom: 10,
  borderBottom: '1px solid #f0f0f0',
  marginBottom: 20,
  fontSize: 20,
  marginTop: 10,
  color: '#777'
};

const sectionContainers = {
  paddingLeft: 15,
  paddingRight:15,
  paddingTop: 60,
  paddingBottom:15
};

const profileLinks = {
  link: {
    marginBottom: 15
  },
  a: {
    textDecoration: 'none',
    color: '#000'
  },
  icon: {
    marginRight: 5,
    top: 5,
    color: '#777'
  }
};

const resumeSections = {
  marginBottom: 30
};

const toolBarStyles = {
  toolbarContainer: {
    paddingTop: 50
  },
  toolbar: {
    background: 'rgb(255, 255, 255)',
    boxShadow: '3px 3px 3px #888888'
  },
  span: {
    width: 30
  },
  textFields: {
    padding: 15
  },
  toolbarSeparator: {
    marginTop: 12
  },
  submitButton: {
    width: 200
  }
};

const todoListStyles = {
  listContainer: {
    marginTop: 30,
    marginLeft: '25%',
    width: '50%',
    background: 'rgb(255, 255, 255)',
    boxShadow: '3px 3px 3px #888888'
  }

};


export {
  h1,
  h2,
  h3,
  h4,
  sectionHeader,
  divider,
  sectionContainers,
  profileLinks,
  resumeSections,
  toolBarStyles,
  todoListStyles
};