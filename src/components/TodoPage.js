import React, {Component, PropTypes} from 'react';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import TodoList from './todo/TodoList'
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import {toolBarStyles} from '../styles/main';


class TodoPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todoItems: [],
      title: '',
      note: '',
      category: ''
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.onListItemChange = this.onListItemChange.bind(this);
    this.onCategoryChange = this.onCategoryChange.bind(this);
    this.onTodoItemRemoved = this.onTodoItemRemoved.bind(this);
    this.handleEditedTodo = this.handleEditedTodo.bind(this);
  }

  onSubmit(){
    let {todoItems, title, note, category} = this.state;
    let tempItems = [].concat(todoItems); //avoid mutating state values directly
    tempItems.push({
      title: title,
      note: note,
      category: category
    });

    this.setState({
      todoItems: tempItems,
      title: '', //clear the title/note/category fields after submitting to allow for another entry 
      note: '',
      category: ''
    });
  }

  onListItemChange(key, value) {
    this.setState({
      [key] : value
    });
  }

  onCategoryChange(event, index, value) {
    this.setState({category: value});
  }

  onTodoItemRemoved(leftOverTodos){
    this.setState({todoItems: leftOverTodos});
  }

  handleEditedTodo(editedData){
    this.setState({todoItems: editedData});
  }
  

  render() {
    let {todoItems, title, note, category} = this.state;
    let submitDisabled = !title.length || !note.length || !category.length;


    return (
      <div className="todo-page" style={toolBarStyles.toolbarContainer}>
        <Toolbar style={toolBarStyles.toolbar}>
          <ToolbarGroup firstChild={true}>
            <TextField
              hintText={'Title'}
              style={toolBarStyles.textFields}
              value={title}
              onChange={(e) => {this.onListItemChange('title', e.target.value);}}
            />
            <span style={toolBarStyles.span}/>
             <TextField
              hintText={'Note'}
              style={toolBarStyles.textFields}
              value={note}
              onChange={(e) => {this.onListItemChange('note', e.target.value);}}
            />
            <span style={toolBarStyles.span}/>
            <SelectField
              hintText={'Type'}
              value={category}
              onChange={this.onCategoryChange}>
              <MenuItem key={1} value="personal" primaryText="Personal"/>
              <MenuItem key={2} value="work" primaryText="Work"/>
            </SelectField>
          </ToolbarGroup>
          
          <ToolbarSeparator style={toolBarStyles.toolbarSeparator}/>

          <ToolbarGroup lastChild={true}>
            <RaisedButton
              primary={true}
              label="Submit"
              onTouchTap={this.onSubmit}
              disabled={submitDisabled}
            />
          </ToolbarGroup>
        </Toolbar>

        <div>
          <TodoList data={todoItems} onTodoRemoved={this.onTodoItemRemoved} onTodoEdited={this.handleEditedTodo}/>
        </div>
      </div>
    );
  }
}


export default TodoPage;
