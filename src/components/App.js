import React, {Component} from "react";
import {Link, Switch, Router, Route} from 'react-router-dom';
import About from './About';
import Resume from './ResumePage';
import Todo from './TodoPage';
import NavBar from './NavBar';
import Grid  from 'react-bootstrap/lib/Grid';

import createBrowserHistory from 'history/createBrowserHistory';

const history = createBrowserHistory({
  basename: '/#/'
});


export default class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {

    return (
      <Router history={history}>
        <div className="app">

          <NavBar/>

          <div className="content" >
            <Grid>
              <Switch>
                {/*<Route exact name="about" path="/" component={About}/>*/}
                <Route exact name="todo" path="/" component={Todo}/>
                <Route name="resume" path="/resume" component={Resume}/>
              </Switch>
            </Grid>
          </div>
        </div>
      </Router>
    );
  }

}