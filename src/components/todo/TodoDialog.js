import React, {Component, PropTypes} from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';

class TodoDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      note: '',
      editedTodo: {}
    };

    this.renderTextFields = this.renderTextFields.bind(this);
    this.onTodoChange = this.onTodoChange.bind(this);
    this.onEditSubmit = this.onEditSubmit.bind(this);

  }

  onEditSubmit(){
    const{onTodoEdited, listItem, listItemIndex} = this.props;
    const{editedTodo} = this.state;

    if(!editedTodo.hasOwnProperty('title')){
      editedTodo.title = listItem.title;
    }
    if(!editedTodo.hasOwnProperty('note')){
      editedTodo.note = listItem.note;
    }
    onTodoEdited(editedTodo, listItemIndex);
  }

  onTodoChange(key, value, listItem) {
    let tempObject = Object.assign({}, listItem);
    tempObject[key] = value;

    this.setState({
      editedTodo : tempObject
    });
  }

  renderTextFields() {
    const {listItem} = this.props;
    let {editedTodo} = this.state;

    return (
      <div className="dialog-textFields" style={{display:'flex', justifyContent: 'space-around'}}>
        <TextField
          floatingLabelText="Title"
          defaultValue={listItem.title}
          onChange={(e) => {this.onTodoChange('title', e.target.value, editedTodo);}}
        />
        <span></span>
        <TextField
          floatingLabelText="Note"
          defaultValue={listItem.note}
          onChange={(e) => {this.onTodoChange('note', e.target.value, editedTodo);}}
        />
      </div>
    );

  }

  render() {
    const {dialogOpen, onCancel} = this.props;

    const dialogActions = [
       <FlatButton
        label="Cancel"
        onTouchTap={onCancel}
        secondary={true}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        onTouchTap={this.onEditSubmit}
      />
    ];


    return (
      <div className="todo-dialog">
        <Dialog
          title="Edit to do item"
          actions={dialogActions}
          modal={false}
          open={dialogOpen}>
          {this.renderTextFields()}
        </Dialog>
      </div>
    );
  }
}


TodoDialog.propTypes = {
  dialogOpen: PropTypes.bool,
  onCancel: PropTypes.func,
  listItem: PropTypes.object,
  onTodoEdited: PropTypes.func,
  listItemIndex: PropTypes.number
};

TodoDialog.defaultPropTypes = {
  dialogOpen: false,
  onCancel: () => {},
  listItem: {},
  onTodoEdited: () => {},
  listItemIndex: 0
};




export default TodoDialog;
