import React, {Component, PropTypes} from 'react';
import {List, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import {grey400, blue500} from 'material-ui/styles/colors';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import ActionAssignment from 'material-ui/svg-icons/action/assignment';
import ActionAccount from 'material-ui/svg-icons/action/assignment-ind'
import Avatar from 'material-ui/Avatar';
import TodoDialog from './TodoDialog';
import {todoListStyles} from './../../styles/main';

const workIcon = (
  <Avatar icon={<ActionAssignment/>} backgroundColor={blue500}/>
);

const personalIcon = (
  <Avatar icon={<ActionAccount/>} backgroundColor={'#5cb85c'}/>
);


class TodoList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openTodoDialog: false,
      listObject: {},
      listItemIndex: 0
    };

    this.renderTodoList = this.renderTodoList.bind(this);
    this.renderSubheader = this.renderSubheader.bind(this);
    this.onDialogOpen = this.onDialogOpen.bind(this);
    this.onDialogClose = this.onDialogClose.bind(this);
    this.onDeleteListItem = this.onDeleteListItem.bind(this);
    this.onTodoEdit = this.onTodoEdit.bind(this);
  }

  onTodoEdit(editedTodoItem, editedIndex) {
    const {data, onTodoEdited} = this.props;
    let editedData = [].concat(data); //avoid mutating props directly
  
    
    for(let index=0; index < data.length; index++) {
      if(index === editedIndex){
        editedData[index] = editedTodoItem;
      }
    }
    
    this.setState({
      openTodoDialog: false
    });

    onTodoEdited(editedData);
  }

  onDeleteListItem(listItem) {
    let {data, onTodoRemoved} = this.props;
    let currentTodos = [].concat(data);
    currentTodos = currentTodos.filter((todoItem) => {
      return todoItem !== listItem;
    });

    onTodoRemoved(currentTodos); //pass current data back to parent to re-render our list items

  }

  onDialogClose() {
    this.setState({openTodoDialog: false});
  }

  onDialogOpen(item, index) {
    this.setState({openTodoDialog: true, listObject: item, listItemIndex: index});
  }

  renderSubheader() {
    const {data} = this.props;
    if (!data.length) {
      return (
        <Subheader>You currently don't have any items in your to do list</Subheader>
      );
    }
    else {
      return (
        <Subheader>Your to do list items</Subheader>
      );
    }
  }

  renderTodoList() {
    const {data} = this.props;

    return data.map((item, index) => {
      const iconButton = (
        <IconButton
          touch={true}
          tooltip="options"
          tooltipPosition="top-right">
          <MoreVertIcon color={grey400}/>
        </IconButton>
      );

      const rightIconMenu = (
        <IconMenu iconButtonElement={iconButton}>
          <MenuItem onClick={() => {this.onDeleteListItem(item);}}>Delete</MenuItem>
          <MenuItem onClick={() => {this.onDialogOpen(item, index);}}>Edit</MenuItem>
        </IconMenu>
      );

      return (
        <div key={index}>
          <ListItem
            leftAvatar={data[index].category === 'work' ? workIcon : personalIcon}
            primaryText={item.title}
            secondaryText={item.note}
            rightIconButton={rightIconMenu}/>
          <Divider/>
        </div>
      );
    });
  }

  render() {
    const {openTodoDialog, listObject, listItemIndex} = this.state;
    const {data} = this.props;

    return (
      <div className="todo-list" style={todoListStyles.listContainer}>
        <List>
          {this.renderSubheader()}
          {this.renderTodoList()}
        </List>

        <TodoDialog
          onCancel={this.onDialogClose}
          dialogOpen={openTodoDialog}
          listItem={listObject}
          listItemIndex={listItemIndex}
          onTodoEdited={this.onTodoEdit}
        />

      </div>
    );
  }
}

TodoList.propTypes = {
  data: PropTypes.array,
  onTodoRemoved: PropTypes.func,
  onTodoEdited: PropTypes.func
};

TodoList.defaultPropTypes = {
  data: [],
  onTodoRemoved: () => {},
  onTodoEdited: () => {}
};


export default TodoList;

