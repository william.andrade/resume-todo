import React, {Component} from 'react';
import resume from './resume/resume.json';
import Profile from './resume/Profile';
import ResumeAbout from './resume/ResumeAbout';
import Work from './resume/Work';
import Skills from './resume/Skills';
import Education from './resume/Education';
import {sectionContainers} from '../styles/main';


class Resume extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div >
        <div className="row">
          <aside style={sectionContainers} className="col-md-4">
            <div className="inner" style={{background: '#fff', padding: 20, boxShadow: '3px 3px 3px #888888'}}>
              <Profile profileObject={resume.basics}/>
            </div>
          </aside>

          <main style={sectionContainers} className="col-md-8">
            <div className="inner" style={{background: '#fff', padding: 20, boxShadow: '3px 3px 3px #888888'}}>
              <ResumeAbout aboutObject={resume.basics.summary}/>
              <Work workArray={resume.work}/>
              <Skills skillsArray={resume.skills}/>
              <Education educationArray={resume.education}/>
            </div>
          </main>
        </div>
      </div>
    );
  }
}


export default Resume;
