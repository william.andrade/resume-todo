import React, {Component, PropTypes} from 'react';
import {h1, h2, divider, profileLinks, profilePic} from './../../styles/main.js';
import profileImage from './../../img/humangeo.jpg';

class Profile extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const profileObject = this.props.profileObject;

    return (
      <div>
        <div className="profileImgs"><img role="presentation" className="img-circle center-block" src={profileImage} width="200" /></div>
          <h1 className="text-center" style={h1}>{profileObject.name}</h1>
          <h2 className="text-center" style={h2}>{profileObject.label}</h2>
          <div className="divider" style={divider}></div>
             <ul className="list-unstyled contact-links text-center">
               <li style={profileLinks.link}>
                 <i className="fa fa-lg fa-location-arrow" style={profileLinks.icon}/>
                 {profileObject.location.city}, {profileObject.location.region}, {profileObject.location.countryCode}
               </li>
               <li style={profileLinks.link}>
                 <i className="fa fa-lg fa-envelope" style={profileLinks.icon}/>
                 <a href={`mailto:${profileObject.email}`}>{profileObject.email}</a>
               </li>
             </ul>
          <div className="divider" style={divider}></div>
             <ul className="profileLinks list-inline text-center">
               <li>
                 <a className="fa fa-lg fa-gitlab fa-2x" style={profileLinks.a} href={"https://gitlab.com/"+profileObject.profiles[0].username}/>
               </li>
             </ul>
          <div className="divider" style={divider}></div>
            <p>This site was built using <a href="https://facebook.github.io/react/">React</a> and a JSON resume schema </p>
      </div>
    );
  }
}

Profile.propTypes = {
  profileObject: PropTypes.object
};


export default Profile;
