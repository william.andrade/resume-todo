import React, {Component, PropTypes} from 'react';
import {resumeSections, sectionHeader} from './../../styles/main';


class Skills extends Component {
  constructor(props) {
    super(props);

    this.getSkills = this.getSkills.bind(this);
  }

  getSkills(){
    const skillsObject = this.props.skillsArray[0];
    const skillKeywords = [];

    for(let i=0; i<skillsObject.keywords.length; i++){
      skillKeywords.push(<li key={i}><span className="label label-success">{skillsObject.keywords[i]}</span></li>);
    }
    return skillKeywords
  }


  render() {

    return (
      <div>
        <section style={resumeSections} className="skills">
          <h2 style={sectionHeader} className="text-uppercase"><i style={{marginRight: 8}} className="fa fa-lg fa-code"/> Skills</h2>
          <ul style={{fontSize: 18}} className="skills-list list-inline">{this.getSkills()}</ul>
        </section>
      </div>
    );
  }
}

Skills.propTypes = {
  skillsArray: PropTypes.array
};

export default Skills;

