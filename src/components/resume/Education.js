import React, {Component, PropTypes} from 'react';
import moment from 'moment';
import {resumeSections, sectionHeader, h3, h4} from './../../styles/main';



class Education extends Component {
  constructor(props) {
    super(props);

    this.getEducation = this.getEducation.bind(this);
  }

  getEducation() {
    const educationObject = this.props.educationArray[0];
    const startDate = moment(educationObject.startDate).format('MMM, YYYY');
    const endDate = moment(educationObject.endDate).format('MMM, YYYY');

    return (
      <div>
        <h3 style={h3}>{educationObject.studyType}</h3>
        <h4 style={h4}>{educationObject.institution}</h4>
        <h5>{educationObject.area}</h5>
        <p>Studied: {startDate} - {endDate}</p>
      </div>
    );
  }

  render() {
    return (
      <div>
        <section style={resumeSections} className="education">
          <h2 style={sectionHeader} className="text-uppercase"><i style={{marginRight: 8}} className="fa fa-lg fa-mortar-board"/> Education</h2>
           {this.getEducation()}
        </section>
      </div>
    );
  }
}

Education.propTypes = {
  educationArray: PropTypes.array
};

export default Education;

