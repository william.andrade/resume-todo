import React, {Component, PropTypes} from 'react';
import moment from 'moment';
import {h3} from './../../styles/main';



class WorkEntry extends Component {
  constructor(props) {
    super(props);

    this.getWorkDates = this.getWorkDates.bind(this);
  }

  getWorkDates(){
    const workObject = this.props.workEntryObject;
    const startDate = moment(workObject.startDate).format('MMM, YYYY');
    let endDate = null;

    if(workObject.endDate === '') {
      endDate = 'Present';
    } else {
      endDate = moment(workObject.endDate).format('MMM, YYYY');
    }

    return <span className="startdate">{startDate} - {endDate}</span>
  }

  render() {
    const workObject = this.props.workEntryObject;

    return (
      <div className="workItem">
        <h3 style={h3}>{workObject.position}, <span>{workObject.company}</span></h3>
        <p className="workDates" style={{color: '#777'}}>{this.getWorkDates()}</p>
        <p>{workObject.summary}</p>
      </div>
    );
  }
}

WorkEntry.propTypes = {
  workEntryObject: PropTypes.object
};

export default WorkEntry;

