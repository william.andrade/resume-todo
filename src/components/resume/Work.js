import React, {Component, PropTypes} from 'react';
import WorkEntry from './WorkEntry';
import {resumeSections, sectionHeader} from './../../styles/main';

class Work extends Component {
  constructor(props) {
    super(props);

    this.getWorkEntries = this.getWorkEntries.bind(this);
  }

  getWorkEntries() {
    const workArray = this.props.workArray;
    const workEntries = [];

    for (let i = 0; i < workArray.length; i++) {
      workEntries.push(<WorkEntry key={i} workEntryObject={workArray[i]}/>);
    }
    return workEntries
  }

  render() {
    return (
      <div>
        <section style={resumeSections} className="work">
          <h2 style={sectionHeader} className="text-uppercase">
            <i style={{marginRight: 8}} className="fa fa-lg fa-building"/>
            Work experience
          </h2>
          {this.getWorkEntries()}
        </section>
      </div>
    );
  }
}

Work.propTypes = {
  workArray: PropTypes.array
};

export default Work;

