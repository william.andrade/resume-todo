import React, {Component, PropTypes} from 'react';
import {resumeSections, sectionHeader, h2} from './../../styles/main';


class ResumeAbout extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const aboutObject = this.props.aboutObject;

    return (
      <div>
        <section style={resumeSections} className="about">
          <h2 style={sectionHeader} className="text-uppercase"><i style={{marginRight: 8}} className="fa fa-lg fa-user"/> About</h2>
            <div>{aboutObject}</div>
        </section>
      </div>
    );
  }
}

ResumeAbout.propTypes = {
  aboutObject: PropTypes.string
};


export default ResumeAbout;
