import React, {Component} from "react";
import Navbar from 'react-bootstrap/lib/Navbar';
import {Link} from 'react-router-dom';


class NavBar extends Component {
  constructor(props) {
    super(props);

  }


  render() {

    return (
      <div>
        <Navbar className="navbar navbar-default navbar-fixed-top">
          <Navbar.Header>
            <Navbar.Brand>Resume To-do</Navbar.Brand>
          </Navbar.Header>
          <ul className="nav navbar-nav">
            {/* Change from a to Link */}
            {/*<li><Link to="/">About</Link></li>*/}
            <li><Link to="/">To-do</Link></li>
            <li><Link to="/resume">Resume</Link></li>
          </ul>
        </Navbar>
      </div>

    );
  }
}

export default NavBar;
