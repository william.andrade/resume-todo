My Project
---
 
Welcome to my first very own ReactJS project. It's a super trivial single page application that implements a Resume page and To do list page. The Resume page is built using react components, a JSON resume schema, and Font Awesome icons. It was heavily inspired by: https://jsonresume.org/schema/ and some examples I found online.
The To do list page is primarily built using components I made that act as wrappers for Material-UI components. The design is simple and something I outlined myself.



Setup/Install dependencies
---
 
```
npm install
```

Compile the app 
---

```
npm run compile
```

Usage
---

Start the development server with this command:

```
npm start
```

 

